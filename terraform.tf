variable "aws_region" {
    type = string
}

variable "domain_name" {
    type = string
}

terraform {
  required_version = ">= 0.12.0"
  backend "s3" {}
}


provider "aws" {
    region = var.aws_region
    version = "~> 2.52"
}

module "marketing-website" {
    source = "./.deploy/terraform/static-site"
    domain_name = var.domain_name
}