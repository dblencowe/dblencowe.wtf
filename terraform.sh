#!/usr/bin/env bash

# Required Environment Variables
: "${DOMAIN?:the domain you want to use for the static site}"

# Environment Variables
: "${AWS_REGION:=us-west-1}"
: "${S3_STATE_KEY:=$DOMAIN.tf}"

command=${1:-plan}
params=${2:-""}

TERRAFORM=`which terraform`
AWS=`which aws`
S3_STATE_BUCKET_NAME="state-$DOMAIN"

if [ $command == "init" ]; then
  if ! $AWS s3api head-bucket --bucket "$S3_STATE_BUCKET_NAME" 2>/dev/null; then
    $AWS s3api create-bucket --acl private --bucket "$S3_STATE_BUCKET_NAME" --region $AWS_REGION --create-bucket-configuration LocationConstraint=$AWS_REGION
    printf "Waiting a minute for the bucket to come available\n"
    sleep 60
  fi

  params=("-input=true" "-backend=true" "-backend-config=bucket=${S3_STATE_BUCKET_NAME}" "-backend-config=key=${S3_STATE_KEY}" "-backend-config=region=${AWS_REGION}")
fi

TF_VAR_domain_name=${DOMAIN} $TERRAFORM $command ${params[*]}