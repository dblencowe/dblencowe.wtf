all: terraform.init domain.check terraform.apply site.deploy

terraform.init:
	./terraform.sh init

domain.check:
	@[ "${DOMAIN}" ] || ( echo ">> DOMAIN environment variable is not set"; exit 1 )

terraform.apply:
	./terraform.sh apply

site.deploy:
	echo "Deploying latest files..."
	aws s3 sync --exclude ".git" dist s3://site-$$DOMAIN

docker: domain.check docker.build docker.deploy docker.clean

docker.build:
	@[ "${AWS_ACCESS_KEY_ID}" ] || ( echo ">> AWS_ACCESS_KEY_ID environment variable is not set"; exit 1 )
	@[ "${AWS_SECRET_ACCESS_KEY}" ] || ( echo ">> AWS_SECRET_ACCESS_KEY environment variable is not set"; exit 1 )
	docker build -f .deploy/Dockerfile --build-arg DOMAIN --build-arg AWS_ACCESS_KEY_ID --build-arg AWS_SECRET_ACCESS_KEY -t $$DOMAIN:latest .

docker.deploy:
	docker run --name $$DOMAIN $$DOMAIN:latest

docker.clean:
	docker rm $$DOMAIN
	docker rmi $$DOMAIN:latest
